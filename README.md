Open-Resty
==========


## reference:
http://openresty.org


## download url 
http://openresty.org/download/ngx_openresty-1.7.7.2.tar.gz



## install open-resty
    tar xzvf ngx_openresty-VERSION.tar.gz
    cd ngx_openresty-VERSION/
    ./configure
    make
    make install

## install lapis framwork

    luarocks install lapis 



## run an example

lua_sample.tar
