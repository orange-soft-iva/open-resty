export PATH=/usr/local/openresty/nginx/sbin:$PATH
# Then we start the nginx server with our config file this way:
nginx -p `pwd`/ -c conf/nginx.conf
