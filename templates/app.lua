local lapis = require("lapis")
local console = require("lapis.console")
local config = require("lapis.config").get()


local app = lapis.Application()
app:enable("etlua")
app.layout = require "views.layout"


app:get("/", function()
  return "Welcome to Lapis " .. require("lapis.version")
end)



app:get("/hello", function()
  return "hello there "
end)


app:get("/index", function(self)
  return { render = "index" }
end)


app:get("/list", function(self)
  self.my_favorite_things = {
    "Cats",
    "Horses",
    "Skateboards"
  }

  return { render = "list" }
end)


app:match("/page/:page", function(self)
  print(self.params.page)
end)



--app:match("/console", console.make())



return app
